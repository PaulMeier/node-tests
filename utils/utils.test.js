const expect = require('expect');
const utils = require('./utils');

describe('Utils', () => {
  describe('#Add', () => {
    it('should add two numbers', () => {
      var result = utils.add(33, 11);
    
      expect(result).toBe(44).toBeA('number');
    });
    
    it('should async add two numbers', (done) => {
      utils.asyncAdd(4, 3, (sum) => {
        expect(sum).toBe(7).toBeA('number');
        done();
      })
    });
  });

  it('should square a number', () => {
    var result = utils.square(4);
  
    expect(result).toBe(16).toBeA('number');
  });
  
  it('should async square a number', (done) => {
    utils.asyncSquare(4, (res) => {
      expect(res).toBe(16).toBeA('number');
      done();
    })
  })

  it('should verify first and last names are set', () => {
    fullName = "Paul Meier";
    user = { location: "Philidelphia", age: 25 };
    var res = utils.setName(user, fullName);
  
    expect(res).toInclude({
      firstName: "Paul",
      lastName: "Meier"
    });
  });
});
// it('should expect some values', () => {
//   expect(12).toNotBe(14);
//   expect({name: "andrew"}).toNotEqual({name: "Andrew"});
//   expect([2,3,4]).toExclude(1);
//   expect({
//     name: "Andrew",
//     age: 25,
//     location: "Philidelphia"
//   }).toExclude({
//     age: 23
//   })
// });