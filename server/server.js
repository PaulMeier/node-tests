const express = require('express');

var app = express();

app.get('/', (req, res) => {
    res.status(404).send({
        error: "Page not found."
    });
})

app.get('/users', (req, res) => {
    res.status(200).send([
        { name: "Paul", age: 35 },
        { name: "Bob", age: 45 },
        { name: "Joe", age: 16 },
        { name: "Jenna", age: 28 },
        { name: "Mary", age: 55 }
    ]);
})

app.listen(3000);

module.exports.app = app;